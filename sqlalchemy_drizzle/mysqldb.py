"""
.. dialect:: drizzle+mysqldb
    :name: MySQL-Python
    :dbapi: mysqldb
    :connectstring: drizzle+mysqldb://<user>:<password>@<host>[:<port>]/<dbname>
    :url: http://sourceforge.net/projects/mysql-python


"""

from .base import (
    DrizzleDialect,
    DrizzleExecutionContext,
    DrizzleCompiler,
    DrizzleIdentifierPreparer)
from sqlalchemy.dialects.mysql.mysqldb import (
    MySQLExecutionContext_mysqldb,
    MySQLCompiler_mysqldb,
    MySQLIdentifierPreparer_mysqldb,
    MySQLDialect_mysqldb)


class DrizzleExecutionContext_mysqldb(MySQLExecutionContext_mysqldb,
                                      DrizzleExecutionContext):
    pass


class DrizzleCompiler_mysqldb(MySQLCompiler_mysqldb, DrizzleCompiler):
    pass


class DrizzleIdentifierPreparer_mysqldb(MySQLIdentifierPreparer_mysqldb,
                                        DrizzleIdentifierPreparer):
    pass


class DrizzleDialect_mysqldb(MySQLDialect_mysqldb, DrizzleDialect):
    execution_ctx_cls = DrizzleExecutionContext_mysqldb
    statement_compiler = DrizzleCompiler_mysqldb
    preparer = DrizzleIdentifierPreparer_mysqldb

    def _detect_charset(self, connection):
        """Sniff out the character set in use for connection results."""

        return 'utf8'


dialect = DrizzleDialect_mysqldb
