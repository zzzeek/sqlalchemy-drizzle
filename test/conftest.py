from sqlalchemy.dialects import registry

registry.register("drizzle", "sqlalchemy_drizzle.mysqldb", "DrizzleDialect_mysqldb")
registry.register("drizzle.mysqldb", "sqlalchemy_drizzle.mysqldb", "DrizzleDialect_mysqldb")


from sqlalchemy.testing.plugin.pytestplugin import *
